public class AvionPasajeros extends Avion {
    /////////Atributo nuevo//////////////
    private int pasajeros;

    //////////Constructor//////////////
    public AvionPasajeros(String color, double tamnho, int pasajeros){
        super(color,tamnho);///////////Hereda los atributos de la superclase
        this.pasajeros = pasajeros;
    }

    /////////Métodos (Acciones)//////////
    public void servir(){
        for (int i=1;i<=pasajeros;i++){
        System.out.println("Atendiendo al pasajero- "+i);
        }
    }
}
