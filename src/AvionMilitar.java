public class AvionMilitar extends Avion{
    /*************
     * Atributos
     ************/
    private int misiles;

    /*************
     * Constructor
     *************/
    public AvionMilitar(String color, double tamanio){
        super(color, tamanio);
    }
    public AvionMilitar(String color, double tamanio, int misiles){
        super(color, tamanio);
        this.misiles = misiles;
    }

    //Modificador
    public void setMisiles(int misiles){
        this.misiles = misiles;
    }

    public void detectar_amenaza(boolean amenaza){
        if(amenaza){
            this.disparar();
        }else{
            System.out.println("No es una amenaza");
        }
    }

    private void disparar(){
        if(this.misiles > 0){
            System.out.println("Disparando...");
            --this.misiles;
        }else{
            System.out.println("No hay munición");
        }
        
    }
}
