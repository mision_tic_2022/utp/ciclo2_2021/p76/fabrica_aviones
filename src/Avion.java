/**
 * Autor:
 * Empresa:
 * Fecha:
 * Ciudad:
 * Descripción:
 */
public class Avion{
    /*************
     * Atributos
     ************/
    private String color;
    private double tamanio;

    /****************
     * Constructores
     ****************/
    public Avion(String color, double tamanio){

    }
    /*
    public Avion(){

    }
    */

    /**************************
     * Métodos
     * (Acciones de la clase)
     **************************/
    public void aterrizar(){
        System.out.println("Aterrizando...");
    }

    public boolean despegar(){
        System.out.println("Despegando...");
        return true;
    }

    public void frenar(){
        System.out.println("Frenar...");
    }
}